# Project Pengarsipan UMS Berbasis Web

[![pipeline status](https://gitlab.com/archazid/webarsipums/badges/master/pipeline.svg)](https://gitlab.com/archazid/webarsipums/commits/master)

Web aplikasi berbasis Django framework untuk sistem pengarsipan di Universitas Muhammadiyah Surakarta

## Ingin menggunakan project ini?

Pastikan Docker terinstall pada sistem operasi.
1. Docker untuk Windows, cek [disini](https://docs.docker.com/docker-for-windows/install/).
2. Docker untuk Mac, cek [disini](https://docs.docker.com/docker-for-mac/install/).
3. Docker untuk Linux, lihat [video](https://www.youtube.com/watch?v=m0TYMte510w&feature=emb_logo).

### Development

Run secara lokal:

```sh
$ docker-compose up -d --build
```

Pastikan [http://localhost:8000/](http://localhost:8000/) menampilkan home page.
