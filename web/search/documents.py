import collections

from django.contrib.auth.models import User
from django.utils.functional import SimpleLazyObject
from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from core.models import Archive


class MyObjectField(fields.ObjectField):
    def get_value_from_instance(self, instance, field_value_to_ignore=None):
        objs = fields.DEDField.get_value_from_instance(
            self, instance, field_value_to_ignore
        )

        if objs is None:
            return {}
        if not isinstance(objs, SimpleLazyObject) and isinstance(
            objs, collections.Iterable
        ):
            return [
                self._get_inner_field_data(obj, field_value_to_ignore)
                for obj in objs
                if obj != field_value_to_ignore
            ]

        return self._get_inner_field_data(objs, field_value_to_ignore)


@registry.register_document
class ArchiveDocument(Document):
    pengunggah = MyObjectField(
        properties={
            "username": fields.TextField(),
            "first_name": fields.TextField(),
            "last_name": fields.TextField(),
        }
    )
    jenis = fields.TextField(attr="get_jenis_display")
    sifat = fields.TextField(attr="get_sifat_display")

    class Index:
        # Name of the Elasticsearch index
        name = "archives"
        # See Elasticsearch Indices API reference for available settings
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = Archive  # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            "nama",
            "deskripsi",
            "tanggal_unggah",
            "publik_mulai",
            "publik_berakhir",
            "file_media",
        ]
        related_models = [User]

        # Paginate the django queryset used to populate the index with the specified size
        # (by default there is no pagination)
        # queryset_pagination = 5000

    def get_queryset(self):
        """
        Not mandatory but to improve performance
        we can select related in one sql request
        """
        return super(ArchiveDocument, self).get_queryset().select_related("pengunggah")

    def get_instances_from_related(self, related_instance):
        """
        If related_models is set,
        define how to retrieve the Archive instance(s) from the related model.
        """
        if isinstance(related_instance, User):
            return related_instance.archive_set.all()
