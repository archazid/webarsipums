import os
from datetime import date
from timeit import default_timer as timer

from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render

SEARCH_ENGINE = os.environ.get("SEARCH_ENGINE", "default")


def search(request):
    # Our input query
    q = request.GET.get("q", "").strip()

    start = timer()
    if q:
        if SEARCH_ENGINE == "elasticsearch":
            # Elasticsearch search
            from elasticsearch_dsl.query import Q
            from .documents import ArchiveDocument

            search = ArchiveDocument.search()
            archive_list = search.query(
                Q("match_phrase", nama=q) | Q("match_phrase", deskripsi=q)
            )

            if archive_list.filter(Q("match", sifat="Publik (temp)")):
                archive_list = archive_list.exclude(
                    Q("range", publik_mulai={"gt": date.today()})
                    | Q("range", publik_berakhir={"lt": date.today()})
                )

            if request.user.is_authenticated:
                username = request.user.username
                archive_list = archive_list.exclude(
                    ~Q("match", pengunggah__username=username)
                    & Q("match", sifat="Pribadi")
                )
            else:
                archive_list = archive_list.filter(Q("match", sifat="Publik"))

            search_count = archive_list.count()
            archive_list = archive_list.to_queryset()
        else:
            # Django queryset
            from django.db.models import Q
            from core.models import Archive

            archive_list = Archive.objects.filter(
                Q(nama__icontains=q) | Q(deskripsi__icontains=q)
            )

            if archive_list.filter(Q(sifat=1)):
                archive_list = archive_list.exclude(
                    Q(publik_mulai__gt=date.today())
                    | Q(publik_berakhir__lt=date.today())
                )

            if request.user.is_authenticated:
                user = request.user
                archive_list = archive_list.exclude(~Q(pengunggah=user) & Q(sifat=3))
            else:
                archive_list = archive_list.filter(Q(sifat=0) | Q(sifat=1))

            search_count = archive_list.count()
    else:
        search_count = 0
        archive_list = ""
    end = timer()
    search_time = end - start

    # Pagination setup
    page = request.GET.get("page")
    paginator = Paginator(archive_list, 10)
    try:
        archives = paginator.page(page)
    except PageNotAnInteger:
        archives = paginator.page(1)
    except EmptyPage:
        archives = paginator.page(paginator.num_pages)

    context = {
        "search_count": search_count,
        "search_time": round(search_time, 2),
        "archives": archives,
    }

    return render(request, "search/search.html", context)
