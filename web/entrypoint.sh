#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.5
    done

    echo "PostgreSQL started."

    if [ "$SEARCH_ENGINE" = "elasticsearch" ]
    then
        echo "Waiting for elasticsearch..."

        while ! nc -z $ES_HOST; do
            sleep 1
        done

        echo "Elasticsearch started."
    fi
fi

python manage.py migrate
# python manage.py search_index --rebuild

exec "$@"
