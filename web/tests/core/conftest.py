# web/tests/core/conftest.py

import uuid

import pytest

from core.models import Archive


@pytest.fixture(scope="function")
def test_password():
    return "superSecretPassword123"


@pytest.fixture(scope="function")
def create_user(db, django_user_model, test_password):
    def _create_user(**kwargs):
        kwargs["password"] = test_password
        if "username" not in kwargs:
            kwargs["username"] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)

    return _create_user


@pytest.fixture(scope="function")
def auto_login_user(db, client, create_user, test_password):
    def _auto_login_user(user=None):
        if user is None:
            user = create_user()
        client.login(username=user.username, password=test_password)
        return client, user

    return _auto_login_user


@pytest.fixture(scope="function")
def add_archive():
    def _add_archive(pengunggah, nama, deskripsi):
        archive = Archive.objects.create(
            pengunggah=pengunggah, nama=nama, deskripsi=deskripsi
        )
        return archive

    return _add_archive
