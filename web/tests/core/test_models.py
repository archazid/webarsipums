# web/tests/core/test_models.py

import pytest

from core.models import Archive


@pytest.mark.django_db
def test_archive_model(create_user):
    username = "userTest"
    user = create_user(username=username)

    archive = Archive(
        pengunggah=user,
        nama="SK UMS No. 1 tentang Test",
        deskripsi="Deskripsi SK Test",
    )
    archive.save()
    assert archive.pengunggah.username == username
    assert archive.nama == "SK UMS No. 1 tentang Test"
    assert archive.deskripsi == "Deskripsi SK Test"
    assert archive.tanggal_unggah
    assert archive.jenis == 0
    assert archive.sifat == 0
    assert archive.file_media == "default-file.pdf"
    assert str(archive) == archive.nama
