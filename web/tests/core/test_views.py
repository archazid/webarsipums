# web/tests/core/test_views.py

import pytest
from django.urls import reverse

from core.models import Archive


@pytest.mark.django_db
def test_home_view(client):
    url = reverse("core:home")
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_add_archive(auto_login_user):
    client, user = auto_login_user()

    archives = Archive.objects.all()
    assert len(archives) == 0

    url = reverse("core:archive_create")
    post = {"nama": "SK UMS No. 1 tentang Test", "deskripsi": "Deskripsi SK Test"}
    response = client.post(url, post, follow=True)

    assert response.status_code == 200
    assert response.content

    archives = Archive.objects.all()
    assert len(archives) == 1


@pytest.mark.django_db
def test_update_archive(auto_login_user, add_archive):
    client, user = auto_login_user()
    archive = add_archive(
        user, "SK Rektor ttg Tata Tertib", "Tata tertib di sekitar lingkungan UMS"
    )

    archives = Archive.objects.all()
    assert len(archives) == 1
    assert archives[0].nama == "SK Rektor ttg Tata Tertib"
    assert archives[0].deskripsi == "Tata tertib di sekitar lingkungan UMS"

    url = reverse("core:archive_update", kwargs={"pk": archive.id})
    post = {"nama": "SK UMS No. 1 tentang Test", "deskripsi": "Deskripsi SK Test"}
    response = client.post(url, post, follow=True)

    assert response.status_code == 200
    assert len(response.context["archive_list"]) == 1
    assert response.context["archive_list"][0].nama == "SK UMS No. 1 tentang Test"
    assert response.context["archive_list"][0].deskripsi == "Deskripsi SK Test"


@pytest.mark.django_db
def test_remove_archive(auto_login_user, add_archive):
    client, user = auto_login_user()
    archive = add_archive(
        user, "SK Rektor ttg Tata Tertib", "Tata tertib di sekitar lingkungan UMS"
    )

    response1 = client.get(reverse("core:archives"))
    assert response1.status_code == 200
    assert response1.context["archive_list"][0] == archive

    response2 = client.delete(
        reverse("core:archive_delete", kwargs={"pk": archive.id}), follow=True
    )

    assert response2.status_code == 200
    assert len(response2.context["archive_list"]) == 0


@pytest.mark.django_db
def test_get_all_archives(auto_login_user, add_archive):
    client, user = auto_login_user()
    archive_one = add_archive(
        pengunggah=user, nama="SK UMS No. 1 tentang Test", deskripsi="Deskripsi SK Test"
    )
    archive_two = add_archive(
        user, "SK Rektor ttg Tata Tertib", "Tata tertib di sekitar lingkungan UMS"
    )

    url = reverse("core:archives")
    response = client.get(url)
    assert response.status_code == 200
    assert len(response.context["archive_list"]) == 2
    assert response.context["archive_list"][0] == archive_one
    assert response.context["archive_list"][1] == archive_two
