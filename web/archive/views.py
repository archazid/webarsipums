from django.views import generic

from core.models import Archive


class ArchiveDetailView(generic.DetailView):
    model = Archive
    template_name = "archive/archive_detail.html"
