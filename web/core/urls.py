from django.urls import path
from django.views import generic

from . import views

app_name = "core"
urlpatterns = [
    path("", generic.TemplateView.as_view(template_name="home.html"), name="home"),
]

urlpatterns += [
    path("dashboard/archives/", views.ArchiveListView.as_view(), name="archives"),
    path(
        "dashboard/archives/<str:username>/",
        views.UserArchiveListView.as_view(),
        name="user_archives",
    ),
    path(
        "dashboard/archive/new/",
        views.ArchiveCreateView.as_view(),
        name="archive_create",
    ),
    path(
        "dashboard/archive/<int:pk>/update/",
        views.ArchiveUpdateView.as_view(),
        name="archive_update",
    ),
    path(
        "dashboard/archive/<int:pk>/delete/",
        views.ArchiveDeleteView.as_view(),
        name="archive_delete",
    ),
]
