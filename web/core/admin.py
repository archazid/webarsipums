from django.contrib import admin
from django.utils.html import format_html

from .models import Archive, Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ("image_tag", "username")
    search_fields = ("user__username",)

    def image_tag(self, obj):
        if obj.image:
            return format_html(
                f'<img src="{obj.image.url}" \
                    style="width: 36px; height: 36px; border-radius: 50%;" \
                    />'
            )
        return "-"

    def username(self, obj):
        return obj.user.username


class ArchiveAdmin(admin.ModelAdmin):
    list_display = ("nama", "jenis", "sifat", "pengunggah")
    list_filter = ("jenis", "tanggal_unggah")
    search_fields = ("nama",)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Archive, ArchiveAdmin)
