import logging
import random
from collections import Counter

import wikipedia
from django.core.management.base import BaseCommand

from core.models import Archive

logger = logging.getLogger("django")
SILENT, NORMAL, VERBOSE, VERY_VERBOSE = 0, 1, 2, 3


class Command(BaseCommand):
    help = "Importing content from wikipedia pages."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument("num_pages", nargs=1, type=int)

    def handle(self, *args, **options):
        verbosity = options.get("verbosity", NORMAL)
        num_pages = options["num_pages"][0]

        if verbosity >= NORMAL:
            self.stdout.write("=== Importing wikipedia pages ===")

        c = Counter()
        wikipedia.set_lang("id")
        for _ in range(0, num_pages):
            wiki = wikipedia.random()
            try:
                user_id = random.randint(1, 3)
                jenis = random.randint(0, 5)
                sifat = random.randint(0, 3)
                wiki_page = wikipedia.page(wiki)
                archive, created = Archive.objects.update_or_create(
                    pengunggah_id=user_id,
                    nama=wiki_page.title,
                    jenis=jenis,
                    sifat=sifat,
                    defaults={"deskripsi": wiki_page.summary},
                )

                if created:
                    c["archives_created"] += 1
            except Exception:
                logger.exception("Failed to index %s", wiki)

            c["archives"] += 1

        if verbosity >= NORMAL:
            self.stdout.write(
                f"Archives processed={c['archives']} (created={c['archives_created']})"
            )
