import os.path
from collections import Counter

from django.core.files import File
from django.core.management.base import BaseCommand
from openpyxl import load_workbook

from core.models import Archive

SILENT, NORMAL, VERBOSE, VERY_VERBOSE = 0, 1, 2, 3


class Command(BaseCommand):
    help = "Imports archive from a local XLSX file."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument("xlsxfile", nargs=1, type=str)
        parser.add_argument("file_basedir", type=str)

    def handle(self, *args, **options):
        verbosity = options.get("verbosity", NORMAL)
        file_path = options["xlsxfile"][0]

        wb = load_workbook(filename=file_path)
        ws = wb.worksheets[0]

        if verbosity >= NORMAL:
            self.stdout.write("=== Importing archives ===")

        index = 0
        c = Counter()
        rows = ws.iter_rows(min_row=2)  # skip the column captions
        for row in rows:
            index += 1
            row_values = [cell.value for cell in row]
            (
                user_id,
                nama,
                deskripsi,
                jenis,
                sifat,
                publik_mulai,
                publik_berakhir,
                filename,
            ) = row_values
            archive, created = Archive.objects.get_or_create(
                pengunggah_id=user_id,
                nama=nama,
                deskripsi=deskripsi,
                jenis=jenis,
                sifat=sifat,
                publik_mulai=publik_mulai,
                publik_berakhir=publik_berakhir,
            )

            with open(os.path.join(options["file_basedir"], filename), "rb") as f:
                archive.file_media = File(f, name=filename)
                archive.save()
                c["archives"] += 1
                if created:
                    c["archives_created"] += 1

        if verbosity >= NORMAL:
            self.stdout.write(
                f"Archives processed={c['archives']} (created={c['archives_created']})"
            )
