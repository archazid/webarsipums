from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.views import generic

from core.models import Archive

# Archive board


class ArchiveListView(LoginRequiredMixin, generic.ListView):
    model = Archive
    template_name = "dashboard/archive_list.html"
    ordering = ["-tanggal_unggah"]

    def get_queryset(self):
        return self.model.objects.all().exclude(sifat=3)


class UserArchiveListView(LoginRequiredMixin, generic.ListView):
    model = Archive
    template_name = "dashboard/user_archives.html"
    ordering = ["-tanggal_unggah"]

    def get_queryset(self):
        return self.model.objects.filter(pengunggah=self.request.user)


class ArchiveCreateView(LoginRequiredMixin, generic.CreateView):
    model = Archive
    template_name = "dashboard/archive_form.html"
    fields = [
        "nama",
        "deskripsi",
        "jenis",
        "sifat",
        "publik_mulai",
        "publik_berakhir",
        "file_media",
    ]
    success_url = reverse_lazy("core:archives")

    def form_valid(self, form):
        form.instance.pengunggah = self.request.user
        return super().form_valid(form)


class ArchiveUpdateView(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    model = Archive
    template_name = "dashboard/archive_form.html"
    fields = [
        "nama",
        "deskripsi",
        "jenis",
        "sifat",
        "publik_mulai",
        "publik_berakhir",
        "file_media",
    ]
    success_url = reverse_lazy("core:archives")

    def test_func(self):
        archive = self.get_object()
        if self.request.user == archive.pengunggah:
            return True
        return False


class ArchiveDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Archive
    success_url = reverse_lazy("core:archives")

    def test_func(self):
        archive = self.get_object()
        if self.request.user == archive.pengunggah:
            return True
        return False
