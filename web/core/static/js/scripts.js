//
// Datatable
//

'use strict';

var DatatableBasic = (function () {

  // Variables
  var $dtBasic = $('#datatable-archive');


  // Methods
  function init($this) {

    // Basic options. For more options check out the Datatables Docs:
    // https://datatables.net/manual/options
    var options = {
      keys: !0,
      select: {
        style: "multi"
      },
      language: {
        paginate: {
          previous: "<i class='fas fa-angle-left'>",
          next: "<i class='fas fa-angle-right'>"
        }
      },
    };

    // Init the datatable
    var table = $this.on('init.dt', function () {
      $('div.dataTables_length select').removeClass('custom-select custom-select-sm');

    }).DataTable(options);
  }


  // Events
  if ($dtBasic.length) {
    init($dtBasic);
  }

})();
