import os

from django.contrib.auth.models import User
from django.contrib.postgres.search import (
    SearchQuery,
    SearchRank,
    SearchVector,
    TrigramSimilarity,
)
from django.db import models
from django.urls import reverse

# from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default="default-avatar.png", upload_to="profiles")

    def __str__(self):
        return f"{self.user.username} Profile"

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)

    #     img = Image.open(self.image.path)

    #     if img.height > 300 or img.width > 300:
    #         output_size = (300, 300)
    #         img.thumbnail(output_size)
    #         img.save(self.image.path)


def media_path(instance, filename):
    """Mengembalikan nama direktori + file untuk unggah sebuah berkas baru"""
    from uuid import uuid4

    ext = filename.split(".")[-1]
    filename = f"{uuid4().hex}.{ext}"

    return os.path.join("archives", filename)


# Search Engine using PostgreSQL search
class ArchiveManager(models.Manager):
    def search(self, text):
        language = "indonesian"
        search_vectors = SearchVector(
            "nama", weight="A", config=language
        ) + SearchVector("deskripsi", weight="B", config=language)
        search_query = SearchQuery(text, config=language)
        search_rank = SearchRank(search_vectors, search_query)
        trigram_similarity = TrigramSimilarity("nama", text)
        return (
            self.get_queryset()
            .annotate(search=search_vectors)
            .filter(search=search_query)
            .annotate(rank=search_rank + trigram_similarity)
            .order_by("-rank")
        )


class Archive(models.Model):
    """
    Model untuk menyimpan berkas arsip
    > nama file harus diubah menjadi 32 karakter acak dan unik,
    > jika nama sudah ada dicari nama lain nama direktori tersembunyi dari user,
    > nama file baru dikembalikan ke user
    """

    pengunggah = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True
    )
    nama = models.CharField(
        max_length=200, help_text="Ketik nama arsip (contoh: SK Rektor No. III)",
    )
    deskripsi = models.TextField(
        blank=True, null=True, help_text="Deskripsi atau ringkasan pendek isi arsip"
    )
    tanggal_unggah = models.DateField(auto_now_add=True)

    JENIS_ARSIP = (
        (0, "Surat Keputusan"),
        (1, "Surat Tugas"),
        (2, "Sertifikat"),
        (3, "Ijazah"),
        (4, "Dokumen"),
        (5, "Foto"),
    )
    jenis = models.IntegerField(
        db_column="cJenis",
        blank=True,
        null=True,
        default=0,
        choices=JENIS_ARSIP,
        help_text="Pilih jenis arsip",
    )

    SIFAT_CHOICES = (
        # dapat diakses seluruh dunia, jika tahu link-nya
        (0, "Publik"),
        # dapat diakses seluruh dunia, sementara dalam rentang tanggal tertentu
        (1, "Publik (temp)"),
        # dapat diakses hanya oleh aplikasi di UMS
        (2, "Internal"),
        # dapat diakses oleh yang mengunggah (atas nama orang/lembaga)
        (3, "Pribadi"),
    )
    sifat = models.IntegerField(
        db_column="cSifat",
        blank=True,
        null=True,
        default=0,
        choices=SIFAT_CHOICES,
        help_text="Pilih sifat arsip",
    )

    publik_mulai = models.DateField(
        blank=True, null=True, help_text="Tanggal arsip mulai bisa diakses publik"
    )
    publik_berakhir = models.DateField(
        blank=True, null=True, help_text="Tanggal arsip terakhir bisa diakses publik"
    )
    file_media = models.FileField(
        "File",
        default="default-file.pdf",
        upload_to=media_path,
        help_text="File fisik arsip",
    )

    objects = ArchiveManager()

    class Meta:
        ordering = ["tanggal_unggah"]

    def extension(self):
        name, extension = os.path.splitext(self.file_media.name)
        return extension[1:].upper()

    def __str__(self):
        """String untuk merepresentasikan object Model."""
        return self.nama

    def get_absolute_url(self):
        return reverse("archive_detail", kwargs={"pk": self.pk})
