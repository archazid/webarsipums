# Base
Django>=2.2,<3.0
django-cleanup>=4.0,<5.0
django-elasticsearch-dsl>=7.0,<8.0
openpyxl
Pillow>=7.0,<8.0
psycopg2>=2.8,<3.0
wikipedia

# Production
boto3>=1.13,<1.14
dj-database-url
django-storages>=1.9,<2.0
gunicorn>=20.0,<21.0
whitenoise>=5.0,<6.0
